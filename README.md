# ohos_coap

## Overview

libcoap is a C implementation of Constrained Application Protocol (CoAP). It is a tool that uses CoAP.
ohos_coap is a third-party library that encapsulates native APIs (NAPIs) of libcoap v4.3.1 and provides CoAP communication capabilities for upper-layer TSs.

Currently, ohos_coap provides APIs to enable the CoAP client to initiate GET, POST, and PUT requests.

It does not support the following capabilities yet:

- Capabilities of the CoAP server
- Sending files and media software from the CoAP client
- Sending TLS/DTLS messages
- Initiating DELETE requests

## How to Use
You can download the library in either of the following ways:
1. Run the following command:

   ```
   https://gitee.com/openharmony-tpc/ohos_coap.git --recurse-submodules
   ```

2. Click **Download** to download the library to the local host, and save the libcoap v4.3.1 code to the **libcoap/src/cpp/libcoapSource** directory.

Run the following command to install the project:

```
ohpm install @ohos/coap
```

OpenHarmony ohpm

For details about the environment configuration, see [Installing the OpenHarmony HAR](https://gitee.com/openharmony-tpc/docs/blob/master/OpenHarmony_har_usage.md).

## Usage Guidelines

### 1. Using a CoAP GET Request

```
// Enable the native log feature.
CoapClient.setNativeLogOpen(true);
// Initiate a CoAP GET request. Replace this.coapUri with the actual URL.
// Each coapClient corresponds to a COAP request task and cannot be reused.
let coapClient = new CoapClient();
let coapGet = coapClient.request(this.coapUri, CoapRequestMethod.GET, CoapRequestType.COAP_MESSAGE_CON);
console.log("libcoap get test");
// Asynchronous callback of the CoAP GET request
coapGet.then((data) => {
  if (data.code == CoapResponseCode.SUCCESS) { // success indicates that the request is successful.
    console.log("libcoap get:" + data.message[0]);
  } else {
    console.log("libcoap get code:" + data.code);
    console.log("libcoap get error message:" + data.message);
  }
})
```

### 2. Using a CoAP PUT Request

```
// Enable the native log feature.
CoapClient.setNativeLogOpen(true);
let coapClient = new CoapClient();
// Set the port number.
coapClient.setPort(5683);
// Set the format of the transparently transmitted data.
coapClient.setFormat(ContentFormat.PLAIN);
// Set the data to be transparently transmitted.
coapClient.setPayload("this is put test payload");
// Initiate a CoAP PUT request. Replace this.coapUri with the actual URL.
// Each coapClient corresponds to a COAP request task and cannot be reused.
let coapPut = coapClient.request(this.coapUri, CoapRequestMethod.PUT, CoapRequestType.COAP_MESSAGE_CON);
console.log("libcoap put test");
// Asynchronous callback of the CoAP GET request
coapPut.then((data) => {
  if (data.code == CoapResponseCode.SUCCESS) { // success indicates that the request is successful.
    console.log("libcoap put:" + data.message[0]);
  } else {
    console.log("libcoap put code:" + data.code);
    console.log("libcoap put error message:" + data.message);
  }
})
```
## Available APIs

| API                                                                       | Input Parameter                                                              | Description                   |
|----------------------------------------------------------------------------|------------------------------------------------------------------|-------------------------|
| request(coapUri: string, method: CoapRequestMethod, type: CoapRequestType) | coapUri: string, method: CoapRequestMethod, type: CoapRequestTyp | Initiates a request.                |
| setFormat(format: ContentFormat)                                           | format: ContentFormat                                            | Sets the format of parameters to be transparently transmitted in a POST or PUT request.|
| setPort(port: number)                                                      | port: number                                                     | Sets the port number.                  |
| setWaitSecond(waitSecond: number)                                          | waitSecond: number                                               | Sets a duration that the client waits for a response.          |
| setObsSecond(obsSecond: number)                                            | obsSecond: number                                                | Sets the duration for connection observation.        |
| setRepeatCount(repeatCount: number)                                        | repeatCount: number                                              | Sets the number of retry requests on the client.            |
| setPayload(payload: string)                                                | payload: string                                                  | Sets the data to be transparently transmitted by the client.             |
| setToken(token: Uint8Array)                                                    | token: Uint8Array                                                    | Sets a token.                |
| setNativeLogOpen(isOpen: boolean)                                          | isOpen: boolean                                                  | Sets whether to enable the log feature on the native side.        |

For details about unit test cases, see [TEST.md](https://gitee.com/openharmony-tpc/ohos_coap/blob/master/TEST.md).
## Constraints

ohos_coap is verified in the following version:

DevEco Studio: 4.0 Release (4.0.3.415), SDK: API10 (4.0.10.5)
DevEco Studio: 5.0 Release (5.0.3.122), SDK: API12 (5.0.0.17)

## Directory Structure

```
|----ohos_coap
|     |---- entry  # Sample code folder
|     |---- libcoap  # ohos_coap library folder
|           |---- index.ets  # External APIs
|     |---- README_CN.md  # Readme file
```

## How to Contribute

If you find any problem during the use, submit an [Issue](https://gitee.com/openharmony-tpc/ohos_coap/issues).
You are also welcome to provide [PRs](https://gitee.com/openharmony-tpc/ohos_coap/pulls).

## Open Source Protocol

The ohos_coap library is based on [Apache License 2.0](https://gitee.com/openharmony-tpc/ohos_coap/blob/master/LICENSE).
The libcoap library is based on [BSD 2](https://gitee.com/openharmony-tpc/ohos_coap/blob/master/LICENSE-BSD2-libcoap).