# the minimum version of CMake.
cmake_minimum_required(VERSION 3.4.1)
project(
  libcoap
  VERSION 4.3.1
  LANGUAGES CXX C)

set(NATIBERENDER_ROOT_PATH ${CMAKE_CURRENT_SOURCE_DIR})

add_subdirectory(boundscheck)

set(LIBCOAP_API_VERSION 3)
set(COAP_LIBRARY_NAME "coap-${LIBCOAP_API_VERSION}")

set(PACKAGE_URL "https://libcoap.net/")
set(PACKAGE_NAME "${PROJECT_NAME}")
set(PACKAGE_TARNAME "${PROJECT_NAME}")
set(PACKAGE_STRING "${PROJECT_NAME} ${PROJECT_VERSION}")
set(PACKAGE_VERSION "${PROJECT_VERSION}")
set(PACKAGE_BUGREPORT "libcoap-developers@lists.sourceforge.net")
set(LIBCOAP_PACKAGE_VERSION "${PACKAGE_VERSION}")
set(LIBCOAP_PACKAGE_URL "${PACKAGE_URL}")
set(LIBCOAP_PACKAGE_NAME "${PACKAGE_NAME}")
set(LIBCOAP_PACKAGE_STRING "${PACKAGE_STRING}")
set(LIBCOAP_PACKAGE_BUGREPORT "${PACKAGE_BUGREPORT}")

set(COAP_DISABLE_TCP 1)
set(COAP_EPOLL_SUPPORT 0)
set(HAVE_ARPA_INET_H 1)
set(HAVE_ASSERT_H 1)
set(HAVE_DLFCN_H 1)
set(HAVE_GETADDRINFO 1)
set(HAVE_GETRANDOM 1)
set(HAVE_IF_NAMETOINDEX 1)
set(HAVE_INTTYPES_H 1)
set(HAVE_LIBCUNIT 0)
set(HAVE_LIBGNUTLS 0)
set(HAVE_LIBTINYDTLS 0)
set(HAVE_LIMITS_H 1)
set(HAVE_MALLOC 1)
set(HAVE_MBEDTLS 0)
set(HAVE_MEMORY_H 1)
set(HAVE_MEMSET 1)
set(HAVE_NETDB_H 1)
set(HAVE_NETINET_IN_H 1)
set(HAVE_NET_IF_H 1)
set(HAVE_OPENSSL 0)
set(HAVE_PTHREAD_H 1)
set(HAVE_PTHREAD_MUTEX_LOCK 1)
set(HAVE_SELECT 1)
set(HAVE_SOCKET 1)
set(HAVE_STDINT_H 1)
set(HAVE_STDLIB_H 1)
set(HAVE_STRCASECMP 1)
set(HAVE_STRINGS_H 1)
set(HAVE_STRING_H 1)
set(HAVE_STRNLEN 1)
set(HAVE_STRRCHR 1)
set(HAVE_SYSLOG_H 1)
set(HAVE_SYS_IOCTL_H 1)
set(HAVE_SYS_SOCKET_H 1)
set(HAVE_SYS_STAT_H 1)
set(HAVE_SYS_TIME_H 1)
set(HAVE_SYS_TYPES_H 1)
set(HAVE_SYS_UNISTD_H 1)
set(HAVE_TIME_H 1)
set(HAVE_UNISTD_H 1)
set(NDEBUG 1)
set(HAVE_ERRNO_H 1)

add_definitions(-DCOAP_RXBUFFER_SIZE=2048)
add_definitions(-DCOAP_DEFAULT_MTU=2048)

configure_file(${CMAKE_CURRENT_LIST_DIR}/libcoapSource/include/coap${LIBCOAP_API_VERSION}/coap.h.in
               ${CMAKE_CURRENT_LIST_DIR}/include/coap${LIBCOAP_API_VERSION}/coap.h)

configure_file(${CMAKE_CURRENT_LIST_DIR}/libcoapSource/cmake_coap_config.h.in
               ${CMAKE_CURRENT_LIST_DIR}/include/coap_config.h)

include_directories(${NATIBERENDER_ROOT_PATH}
                    ${NATIBERENDER_ROOT_PATH}/libcoapSource/include
                    ${NATIBERENDER_ROOT_PATH}/include
                    ${NATIBERENDER_ROOT_PATH}/log
                    ${NATIBERENDER_ROOT_PATH}/boundscheck)

add_library(coap SHARED native-bridge.cpp
                        coap-client.cpp
                        tools.cpp
                        ${NATIBERENDER_ROOT_PATH}/log/ohos_log.cpp
                        ${NATIBERENDER_ROOT_PATH}/libcoapSource/src/coap_address.c
                        ${NATIBERENDER_ROOT_PATH}/libcoapSource/src/coap_async.c
                        ${NATIBERENDER_ROOT_PATH}/libcoapSource/src/coap_block.c
                        ${NATIBERENDER_ROOT_PATH}/libcoapSource/src/coap_asn1.c
                        ${NATIBERENDER_ROOT_PATH}/libcoapSource/src/coap_io.c
                        ${NATIBERENDER_ROOT_PATH}/libcoapSource/src/coap_cache.c
                        ${NATIBERENDER_ROOT_PATH}/libcoapSource/src/coap_debug.c
                        ${NATIBERENDER_ROOT_PATH}/libcoapSource/src/coap_event.c
                        ${NATIBERENDER_ROOT_PATH}/libcoapSource/src/coap_hashkey.c
                        ${NATIBERENDER_ROOT_PATH}/libcoapSource/src/coap_notls.c
                        ${NATIBERENDER_ROOT_PATH}/libcoapSource/src/coap_prng.c
                        ${NATIBERENDER_ROOT_PATH}/libcoapSource/src/coap_session.c
                        ${NATIBERENDER_ROOT_PATH}/libcoapSource/src/coap_tcp.c
                        ${NATIBERENDER_ROOT_PATH}/libcoapSource/src/coap_time.c
                        ${NATIBERENDER_ROOT_PATH}/libcoapSource/src/coap_encode.c
                        ${NATIBERENDER_ROOT_PATH}/libcoapSource/src/coap_mem.c
                        ${NATIBERENDER_ROOT_PATH}/libcoapSource/src/coap_net.c
                        ${NATIBERENDER_ROOT_PATH}/libcoapSource/src/coap_option.c
                        ${NATIBERENDER_ROOT_PATH}/libcoapSource/src/coap_pdu.c
                        ${NATIBERENDER_ROOT_PATH}/libcoapSource/src/coap_resource.c
                        ${NATIBERENDER_ROOT_PATH}/libcoapSource/src/coap_str.c
                        ${NATIBERENDER_ROOT_PATH}/libcoapSource/src/coap_subscribe.c
                        ${NATIBERENDER_ROOT_PATH}/libcoapSource/src/coap_uri.c
                        ${NATIBERENDER_ROOT_PATH}/libcoapSource/src/coap_ws.c
                        ${NATIBERENDER_ROOT_PATH}/libcoapSource/src/coap_oscore.c
                        ${NATIBERENDER_ROOT_PATH}/libcoapSource/src/coap_netif.c
                        ${NATIBERENDER_ROOT_PATH}/libcoapSource/src/coap_dtls.c
                        ${NATIBERENDER_ROOT_PATH}/libcoapSource/src/coap_layers.c)

target_link_libraries(coap PUBLIC boundscheck libace_napi.z.so libhilog_ndk.z.so
                        -Wl,--build-id -Wl,-z,relro -Wl,-z,now -Wl,-z,noexecstack -fPIE -s -Wl,
                        -Bsymbolic -rdynamic)
