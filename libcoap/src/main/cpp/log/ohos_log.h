/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef OHOS_LOG_H
#define OHOS_LOG_H
#include <cstdlib>
#include "napi/native_api.h"
enum class CoapLogLevel {
    IL_INFO,
    IL_DEBUG,
    IL_WARN,
    IL_ERROR,
    IL_FATAL
};

#define LOGD(...) OhosLogPrint(CoapLogLevel::IL_DEBUG, "libcoap", __VA_ARGS__)
#define LOGI(...) OhosLogPrint(CoapLogLevel::IL_INFO, "libcoap", __VA_ARGS__)
#define LOGW(...) OhosLogPrint(CoapLogLevel::IL_WARN, "libcoap", __VA_ARGS__)
#define LOGE(...) OhosLogPrint(CoapLogLevel::IL_ERROR, "libcoap", __VA_ARGS__)
#define LOGF(...) OhosLogPrint(CoapLogLevel::IL_FATAL, "libcoap", __VA_ARGS__)

#ifdef __cplusplus
extern "C" {
#endif
void OhosLogPrint(enum CoapLogLevel level, const char *tag, const char *fmt, ...);
namespace OhosLog {
napi_value SetLogOpen(napi_env env, napi_callback_info info);
}

#ifdef __cplusplus
}
#endif
#endif