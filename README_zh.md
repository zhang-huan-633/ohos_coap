# ohos_coap

## 介绍

libcoap是Coap协议的C语言实现，它是使用Coap协议的工具。
ohos_coap是基于libcoap v4.3.1版本，封装napi接口，给上层ts提供coap通信能力的三方库。

目前实现的能力：

- 支持coap客户端发起get,post,put请求

暂不支持的能力：

- coap服务端相关能力
- coap客户端发送文件，媒体软件能力
- 不支持tls/dtls的消息
- 不支持发起 delete请求

## 使用本工程
有两种方式可以下载本工程：
1. 开发者如果想要使用本工程,可以使用git命令
```
git clone https://gitee.com/openharmony-tpc/ohos_coap.git --recurse-submodules
```
2. 点击下载按钮，把本工程下到本地，再把libcoap v4.3.1版本代码放入libcoap/src/cpp/libcoapSource目录下，这样才可以编译通过。

3. 漏洞修复，进入到libcoap\src\main\cpp\thirdModule 目录下，执行 modify.sh 脚本，将本目录下的 patch 文件合入到 libcoap 源码中。
   详情查看[漏洞修复记录](https://gitee.com/openharmony-tpc/ohos_coap/blob/master/Vulnerability_Repair.md)

## 下载安装

```
ohpm install @ohos/coap
```

OpenHarmony ohpm
环境配置等更多内容，请参考[如何安装 OpenHarmony ohpm 包](https://gitee.com/openharmony-tpc/docs/blob/master/OpenHarmony_har_usage.md)

## 使用

### 1、coap get 请求展示

```
// 打开native log开关
CoapClient.setNativeLogOpen(true);
// 发起coap get请求，注意this.coapUri请使用者换成真实的url
// 每一个CoapClient对应一个COAP请求任务，不可复用
let coapClient = new CoapClient();
let coapGet = coapClient.request(this.coapUri, CoapRequestMethod.GET, CoapRequestType.COAP_MESSAGE_CON);
console.log("libcoap get test");
//coap get请求异步回调
coapGet.then((data) => {
  if (data.code == CoapResponseCode.SUCCESS) { //sucess说明请求成功 
    console.log("libcoap get:" + data.message[0]);
  } else {
    console.log("libcoap get code:" + data.code);
    console.log("libcoap get error message:" + data.message);
  }
})
```

### 2、coap put 请求展示

```
// 打开native log开关
CoapClient.setNativeLogOpen(true);
let coapClient = new CoapClient();
//设置端口号
coapClient.setPort(5683);
//设置透传数据的格式
coapClient.setFormat(ContentFormat.PLAIN);
//设置透传数据
coapClient.setPayload("this is put test payload");
// 发起coap put请求，注意this.coapUri请使用者换成真实的url
// 每一个CoapClient对应一个COAP请求任务，不可复用
let coapPut = coapClient.request(this.coapUri, CoapRequestMethod.PUT, CoapRequestType.COAP_MESSAGE_CON);
console.log("libcoap put test");
//coap get请求异步回调
coapPut.then((data) => {
  if (data.code == CoapResponseCode.SUCCESS) { //sucess说明请求成功 
    console.log("libcoap put:" + data.message[0]);
  } else {
    console.log("libcoap put code:" + data.code);
    console.log("libcoap put error message:" + data.message);
  }
})
```
## 接口说明

| 方法名                                                                        | 入参                                                               | 接口描述                    |
|----------------------------------------------------------------------------|------------------------------------------------------------------|-------------------------|
| request(coapUri: string, method: CoapRequestMethod, type: CoapRequestType) | coapUri: string, method: CoapRequestMethod, type: CoapRequestTyp | 客户端发起请求                 |
| setFormat(format: ContentFormat)                                           | format: ContentFormat                                            | 客服端发起post,put请求时透传参数的格式 |
| setPort(port: number)                                                      | port: number                                                     | 设置端口号                   |
| setWaitSecond(waitSecond: number)                                          | waitSecond: number                                               | 客户端设置请求等待超时时间           |
| setObsSecond(obsSecond: number)                                            | obsSecond: number                                                | 客户端设置连接持续观察等待时间         |
| setRepeatCount(repeatCount: number)                                        | repeatCount: number                                              | 客户端设置重试请求次数             |
| setPayload(payload: string)                                                | payload: string                                                  | 设置客服端透传的内容              |
| setToken(token: Uint8Array)                                                | token: Uint8Array                                                | 设置token                 |
| setNativeLogOpen(isOpen: boolean)                                          | isOpen: boolean                                                  | 是否打开native侧的log         |
| addOption(optionNum: number, value: string)                                | optionNum: number, value: string                                 | 客户端自定义新增一个option选项       |
| setMid(mid: number)                                          	             | mid: number                                                      | 客户端自定义报文的message id         |
| setPayloadBinary(payload: Uint8Array)                                      | payload: Uint8Array                                              | 客户端透传的字节内容  

单元测试用例详情见[TEST.md](https://gitee.com/openharmony-tpc/ohos_coap/blob/master/TEST.md)
## 约束与限制

在下述版本验证通过：

DevEco Studio: 4.0 Release (4.1.3.500), SDK: API10 (4.0.10.5)
DevEco Studio: 5.0 Release (5.0.3.122), SDK: API12 (5.0.0.17)

## 目录结构

```
|----ohos_coap
|     |---- entry  # 示例代码文件夹
|     |---- libcoap  # ohos_coap库文件夹
|           |---- index.ets  # 对外接口
|     |---- README_CN.md  # 使用说明文档
```

## 贡献代码

使用过程中发现任何问题都可以提 [Issue](https://gitee.com/openharmony-tpc/ohos_coap/issues)
给我们，当然，我们也非常欢迎你给我们发 [PR](https://gitee.com/openharmony-tpc/ohos_coap/pulls) 。

## 开源协议

本项目基于 [Apache License 2.0 ](https://gitee.com/openharmony-tpc/ohos_coap/blob/master/LICENSE) ，请自由地享受和参与开源。
依赖libcoap基于 [BSD 2](https://gitee.com/openharmony-tpc/ohos_coap/blob/master/LICENSE-BSD2-libcoap) ，请自由地享受和参与开源。